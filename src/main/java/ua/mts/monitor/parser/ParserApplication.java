package ua.mts.monitor.parser;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Парсер.
 * Основной класс приложения, который собирает вместе все компоненты
 * и запускает Flow-ы
 */
@SpringBootApplication
@EnableBatchProcessing
@ImportResource({
        "classpath:ftp-download-flow.xml",
        "classpath:email-error-flow.xml",
        "classpath:parser-flow.xml",
        "classpath:parser-batch.xml"
})
public class ParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParserApplication.class, args);
    }

}
