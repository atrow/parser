package ua.mts.monitor.parser;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;
import ua.mts.monitor.parser.model.Counter;

import java.util.HashMap;
import java.util.Map;

public class CounterFieldSetMapper implements FieldSetMapper<Counter> {

	@Override
	public Counter mapFieldSet(FieldSet fieldSet) throws BindException {

        String[] values = fieldSet.getValues();
        Map<String, String> map = new HashMap<String, String>(values.length/2);

        for (int i = 0; i < values.length / 2; i+=2) {
            String key = values[i].trim();
            String val = values[i+1].trim();
            map.put(key, val);
        }

		Counter counter = new Counter();

        counter.setCountersMap(map);

		return counter;
	}
}
