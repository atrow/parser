package ua.mts.monitor.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import ua.mts.monitor.parser.model.Counter;

import java.util.List;

public class CounterWriter implements ItemWriter<Counter> {

    private static final Logger log = LoggerFactory.getLogger(CounterWriter.class);

	@Override
	public void write(List<? extends Counter> counters) throws Exception {
        for (Counter counter : counters) {
            log.warn("Processed counter for BSC {} at {}", counter.getBscNumber(), counter.getPeriodRealStartTime());
        }
    }
}
