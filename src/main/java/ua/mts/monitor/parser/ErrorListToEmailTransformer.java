package ua.mts.monitor.parser;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import java.util.List;

/**
 * Конструирует текст емейла на основе списка исключений.
 * Также добавляет полезные заголовки, исходя из того, что
 * все исключения в списке одинаковые.
 *
 * В случае пустого списка (не должно происходить) добавляет заголовок size = 0
 */
public class ErrorListToEmailTransformer {

    @Transformer
    public Message<String> transformExecutionsToMail(List<Throwable> exceptions) {

        if (exceptions == null || exceptions.isEmpty()) {
            return MessageBuilder
                    .withPayload("")
                    .setHeader("size", 0)
                    .build();
        }

        Integer size = exceptions.size();
        Throwable throwable = exceptions.get(0);
        String message = throwable.getMessage();
        String stackTrace = ExceptionUtils.getStackTrace(throwable);

        String result = String.format("There are %s exceptions occurred, with message:\n%s\n\nStackTrace:\n%s", size, message, stackTrace);
        return MessageBuilder
                .withPayload(result)
                .setHeader("size", size)
                .setHeader("message", message)
                .setHeader("stackTrace", stackTrace)
                .build();
    }

}
