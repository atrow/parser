package ua.mts.monitor.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.listener.ItemListenerSupport;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import ua.mts.monitor.parser.model.Counter;

import java.util.List;

public class LoadCounterListener extends ItemListenerSupport<Counter, Counter> {

    protected static final Logger log = LoggerFactory.getLogger(LoadCounterListener.class);

	@Autowired
	@Qualifier("rawErrorChannel")
    private MessageChannel rawErrorChannel;

	@Override
	public void onReadError(Exception ex) {
		if (ex instanceof FlatFileParseException) {
			FlatFileParseException ffpe = (FlatFileParseException) ex;
			log.error("Error reading data on line '{}' - data: '{}'", ffpe.getLineNumber(), ffpe.getInput());
		} else {
            log.error("Error reading data : '{}'", ex.getMessage());
        }
        rawErrorChannel.send(MessageBuilder.withPayload(ex).build());
	}

	@Override
	public void onWriteError(Exception ex, List<? extends Counter> item) {
        log.error("Error writing data : '{}'", ex.getMessage());
        rawErrorChannel.send(MessageBuilder.withPayload(ex).build());
	}
}

