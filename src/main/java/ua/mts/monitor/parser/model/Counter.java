package ua.mts.monitor.parser.model;

import java.util.Date;
import java.util.Map;

/**
 * Для совместимости пока так
 */
public class Counter {

    private Integer number;

    private String bscNumber;

    private Date periodRealStartTime;

    private Map<String, String> countersMap;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getBscNumber() {
        return bscNumber;
    }

    public void setBscNumber(String bscNumber) {
        this.bscNumber = bscNumber;
    }

    public Date getPeriodRealStartTime() {
        return periodRealStartTime;
    }

    public void setPeriodRealStartTime(Date periodRealStartTime) {
        this.periodRealStartTime = periodRealStartTime;
    }

    public Map<String, String> getCountersMap() {
        return countersMap;
    }

    public void setCountersMap(Map<String, String> countersMap) {
        this.countersMap = countersMap;
    }
}
