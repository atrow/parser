package ua.mts.monitor.parser;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import ua.mts.monitor.parser.support.FtpServerManager;

import java.io.File;
import java.io.InputStream;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class FtpFlowTest extends AbstractSpringTest {

    protected static final Logger log = LoggerFactory.getLogger(FtpFlowTest.class);

    private static FtpServerManager ftpServerManager = new FtpServerManager();

    @Autowired
    PollableChannel ftpChannel;

    @Value("${parser.ftp.port.1}") Integer port1;
    @Value("${parser.ftp.username.1}") String user1;
    @Value("${parser.ftp.password.1}") String pass1;
    @Value("${parser.ftp.local.dir.1.1}") String localDir11;
    @Value("${parser.ftp.local.dir.1.2}") String localDir12;

    @Value("${parser.ftp.port.2}") Integer port2;
    @Value("${parser.ftp.username.2}") String user2;
    @Value("${parser.ftp.password.2}") String pass2;
    @Value("${parser.ftp.local.dir.2.1}") String localDir21;
    @Value("${parser.ftp.local.dir.2.2}") String localDir22;

    private String ftpRemoteDir1;
    private String ftpRemoteDir2;

    @Before
    public void runFtpServers() throws Exception {

        ftpRemoteDir1 = "target/ftp/server1";
        ftpRemoteDir2 = "target/ftp/server2";

        uploadFiles("test-files/ftp1", ftpRemoteDir1,
                "BSC386549.1.201502270400.134458",
                "BSC386549.1.201502270500.134882",
                "BSC386549.98.201502270500.134678",
                "BSC386549.98.201502270600.135604");

        uploadFiles("test-files/ftp2", ftpRemoteDir2,
                "BSC376269.1.201502270000.928010",
                "BSC376269.1.201502270100.928495",
                "BSC376269.98.201502270000.927854",
                "BSC376269.98.201502270100.928268");

        if (!ftpServerManager.hasStartedServers()) {

            ftpServerManager.startNewServer(ftpRemoteDir1, port1, user1, pass1);
            ftpServerManager.startNewServer(ftpRemoteDir2, port2, user2, pass2);
        }
    }

	@Test
	public void should_receive_8_files_from_2_servers() throws Exception {

        Thread.sleep(1000000);

        // Get 4 messages which must contain files
        List<Message<?>> receivedFileMessages = new ArrayList<Message<?>>();
        for (int i = 0; i < 8; i++) {
            Message<?> message = ftpChannel.receive(20000);
            log.info(String.format("Received file message: %s.", message));
            assertNotNull(message);
            receivedFileMessages.add(message);
        }

        // Get 2 more messages which must NOT contain files
        for (int i = 0; i < 2; i++) {
            Message<?> message = ftpChannel.receive(2000);
            log.info(String.format("Received empty message: %s.", message));
            assertNull(message);
        }

        Set<String> receivedFileNames = new HashSet<String>();
        for (Message<?> receivedFileMessage : receivedFileMessages) {
            receivedFileNames.add(((File)receivedFileMessage.getPayload()).getName());
        }

        List<String> fileNames = Arrays.asList(
                "BSC386549.1.201502270400.134458",
                "BSC386549.1.201502270500.134882",
                "BSC386549.98.201502270500.134678",
                "BSC386549.98.201502270600.135604",
                "BSC376269.1.201502270000.928010",
                "BSC376269.1.201502270100.928495",
                "BSC376269.98.201502270000.927854",
                "BSC376269.98.201502270100.928268"
        );

        assertTrue(receivedFileNames.containsAll(fileNames));

        File file;
        file = new File(localDir11, "BSC386549.1.201502270400.134458"); assertTrue(file.exists());
        file = new File(localDir11, "BSC386549.1.201502270500.134882"); assertTrue(file.exists());
        file = new File(localDir12, "BSC386549.98.201502270500.134678"); assertTrue(file.exists());
        file = new File(localDir12, "BSC386549.98.201502270600.135604"); assertTrue(file.exists());
        file = new File(localDir21, "BSC376269.1.201502270000.928010"); assertTrue(file.exists());
        file = new File(localDir21, "BSC376269.1.201502270100.928495"); assertTrue(file.exists());
        file = new File(localDir22, "BSC376269.98.201502270000.927854"); assertTrue(file.exists());
        file = new File(localDir22, "BSC376269.98.201502270100.928268"); assertTrue(file.exists());
    }


    private void uploadFiles(String sourceClassPathDir, String destinationDir, String ... fileNames) throws Exception {
        for (String sourceFileName : fileNames) {

            File targetFile = new File(destinationDir, sourceFileName);

            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(sourceClassPathDir + File.separator + sourceFileName);
            FileUtils.copyInputStreamToFile(inputStream, targetFile);

//            boolean success = targetFile.setLastModified(1425757800000L);
//            assertTrue(success);

            assertTrue(targetFile.exists());
        }
    }

    @AfterClass
    public static void stopFtpServers() {
        ftpServerManager.shutDownServers();
    }

}
