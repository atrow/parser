package ua.mts.monitor.parser;

import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.mts.monitor.parser.support.SpringStubConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        ParserApplication.class, SpringStubConfig.class
})
@ActiveProfiles("test")
public abstract class AbstractSpringTest {
    // Empty
}
