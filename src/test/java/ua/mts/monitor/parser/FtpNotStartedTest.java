package ua.mts.monitor.parser;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import ua.mts.monitor.parser.support.StubJavaMailSender;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class FtpNotStartedTest extends AbstractSpringTest {

    protected static final Logger log = Logger.getLogger(FtpNotStartedTest.class);

    @Autowired
    PollableChannel ftpChannel;

    @Autowired
    JavaMailSender mailSender;


	@Test
	public void should_not_receive_ftp_message() {
        Message<?> message = ftpChannel.receive(2000);
        assertNull(message);
	}

	@Test
	public void should_receive_email_with_errors() {
        Message<?> message = ftpChannel.receive(3000);
        assertNull(message);

        StubJavaMailSender stubMailSender = (StubJavaMailSender) mailSender;

        int emails = stubMailSender.getSentSimpleMailMessages().size();

        assertEquals("One mail per flow should be received in 3 seconds", 2, emails);
	}

}
