package ua.mts.monitor.parser.support;

import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.usermanager.ClearTextPasswordEncryptor;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.AbstractUserManager;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.util.Arrays;

/**
 * Менеджер пользователей для встроенного тестового FTP-сервера.
 * Работает с одним не-анонимным пользователем
 */
public class TestUserManager extends AbstractUserManager {

	private BaseUser testUser;

    private String testUserUsername;
    private String testUserPassword;

	public TestUserManager(String homeDirectory, String username, String password) {
		super("admin", new ClearTextPasswordEncryptor());

        testUserUsername = username;
        testUserPassword = password;

		testUser = new BaseUser();
		testUser.setAuthorities(Arrays.asList(new ConcurrentLoginPermission(10, 10), new WritePermission()));
		testUser.setEnabled(true);
		testUser.setHomeDirectory(homeDirectory);
		testUser.setMaxIdleTime(10000);
		testUser.setName(testUserUsername);
		testUser.setPassword(testUserPassword);
	}

	public User getUserByName(String username) throws FtpException {
		if(testUserUsername.equals(username)) {
			return testUser;
		}
		return null;
	}

	public String[] getAllUserNames() throws FtpException {
		return new String[] {testUserUsername};
	}

	public void delete(String username) throws FtpException {
		throw new UnsupportedOperationException("Deleting of FTP Users is not supported.");
	}

	public void save(User user) throws FtpException {
		throw new UnsupportedOperationException("Saving of FTP Users is not supported.");
	}

	public boolean doesExist(String username) throws FtpException {
        return testUserUsername.equals(username);
	}

	public User authenticate(Authentication authentication) throws AuthenticationFailedException {
		if(UsernamePasswordAuthentication.class.isAssignableFrom(authentication.getClass())) {
			UsernamePasswordAuthentication upAuth = (UsernamePasswordAuthentication) authentication;

			if(testUserUsername.equals(upAuth.getUsername()) && testUserPassword.equals(upAuth.getPassword())) {
				return testUser;
			}
		}
		return null;
	}
}
