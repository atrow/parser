package ua.mts.monitor.parser.support;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Управляет встроенными тестовыми FTP-серверами
 */
public class FtpServerManager {

	protected static final Logger log = LoggerFactory.getLogger(FtpServerManager.class);

	private List<FtpServer> servers = new ArrayList<FtpServer>();

    public void startNewServer(String ftpRootDir, Integer port, String username, String password) {

        log.info("Creating new FTP server on port... {}", port);

        File ftpRoot = new File (ftpRootDir);
        boolean success = ftpRoot.mkdirs();

        TestUserManager userManager = new TestUserManager(ftpRoot.getAbsolutePath(), username, password);

        FtpServerFactory serverFactory = new FtpServerFactory();
        serverFactory.setUserManager(userManager);

        ListenerFactory factory = new ListenerFactory();
        factory.setPort(port);

        serverFactory.addListener("default", factory.createListener());

        FtpServer server = serverFactory.createServer();
        servers.add(server);

        try {
            server.start();
        } catch (FtpException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean hasStartedServers() {
        for (FtpServer server : servers) {
            if (!server.isStopped()) {
                return true;
            }
        }
        return false;
    }

    public void shutDownServers() {
        for (FtpServer server : servers) {
            server.stop();
        }
    }

}
