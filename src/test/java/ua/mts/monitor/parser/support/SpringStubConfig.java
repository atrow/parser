package ua.mts.monitor.parser.support;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Дополнительная конфигурация для тестов.
 * Подменяет бины, определенные в основной конфигурации, тестовыми заглушками,
 * перечисленными в ImportResource
 */
@Configuration
@ImportResource({
        "classpath:stub-testing-beans.xml"
})
public class SpringStubConfig {
    // Empty
}
